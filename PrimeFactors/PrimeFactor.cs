﻿using System;

namespace PrimeFactors
{
    public static class PrimeFactor
    {
        public static int[] GetFactors(int number)
        {
            if (number <= 0)
            {
                throw new ArgumentException("mistake");
            }

            int div = 2;
            int res = 0;
            int n = number;
            int[] buf = new int[7];

            while (n > 1)
            {
                while (n % div == 0)
                {
                    buf[res] = div;
                    res++;
                    n /= div;
                }

                div++;
            }

            int[] result = new int[res];
            while (res > 0)
            {
                result[res - 1] = buf[res - 1];
                res--;
            }

            return result;
        }
    }
}
